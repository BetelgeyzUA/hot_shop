<?php

use yii\helpers\Html;

?>
<?php if(!empty($session['cart'])): ?>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Фото</th>
                <th>SKU</th>
                <th>Наименование</th>
                <th>Кол-во</th>
                <th>Цена</th>
                <th>Сумма</th>
                <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($session['cart'] as $id => $item):?>
                <tr>
                    <td><?= Html::img('@web/'.$item['img'], ["alt" => $item['title'] ]) ?></td>
                    <td><?= $item['SKU']?></td>
                    <td><?= $item['title']?></td>
                    <td><?= $item['quantity']?></td>
                    <td><?= $item['price']?></td>
                    <td><?= $item['quantity'] * $item['price']?></td>
                    <td><span data-id="<?= $id ?>" class="glyphicon glyphicon-remove text-danger del-item " aria-hidden="true"></span></td>
                </tr>
            <?php endforeach?>
            <tr>
                <td colspan="3">Итого: </td>
                <td colspan="2"><?= $session['cart.quantity']?></td>
                <td><?= $session['cart.sum']?></td>
                <td></td>

            </tr>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <h3>Корзина пуста</h3>
<?php endif;?>