<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="container" id="view_cart">
    <?php if( Yii::$app->session->hasFlash('success') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif;?>

    <?php if( Yii::$app->session->hasFlash('error') ): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif;?>

    <?php if(!empty($session['cart'])): ?>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Фото</th>
                    <th>Наименование</th>
                    <th>SKU</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($session['cart'] as $id => $item):?>
                    <tr>
                        <td><?= Html::img('@web/'.$item['img'])?></td>
                        <td><?= Html::a($item['title'], ['product/view', 'id' => $id]) ?></td>
                        <td><?= $item['SKU']?></td>
                        <td><?= $item['quantity']?></td>
                        <td><?= $item['price']?></td>
                        <td><?= Html::a('<span data-id="<?= $id ?>" class="glyphicon glyphicon-remove text-danger del-item " aria-hidden="true"></span>', ['cart/del-item', 'id' => $id]) ?></td>
                    </tr>
                <?php endforeach?>
                <tr>
                    <td colspan="3">Итого: </td>
                    <td><?= $session['cart.quantity']?></td>
                    <td><?= $session['cart.sum']?></td>
                    <td></td>

                </tr>
                </tbody>
            </table>
            <hr/>

            <?php $form = ActiveForm::begin(['id' => 'orderAddress']) ?>
                <?= $form->field($order, 'username') ?>
                <?= $form->field($order, 'email') ?>
                <?= $form->field($order, 'phone') ?>
                <?= $form->field($order, 'address') ?>
                <?= Html::submitButton('order', ['class' => 'btn btn-success']) ?>
            <?php ActiveForm::end() ?>
        </div>
    <?php else: ?>
        <h3>Корзина пуста</h3>
    <?php endif;?>

</div>
