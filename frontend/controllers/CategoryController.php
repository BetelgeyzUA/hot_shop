<?php

namespace frontend\controllers;

use yii\base\Controller;
use common\models\Category;
use common\models\Product;
use Yii;
use yii\data\Pagination;

class CategoryController extends Controller
{
        public function actionIndex() {

            $hits = Product::find()->limit(6)->orderBy('created_at DESC')->all();

            return $this->render('index', compact('hits'));

        }

        public function actionView() {

            $id = Yii::$app->request->get('id');

//            $products = Product::find()->where(['id_category' => $id])->all();
            $query = Product::find()->where(['id_category' => $id]);
            $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3, 'forcePageParam' => false, 'pageSizeParam' => false]);
            $products = $query->offset($pages->offset)->limit($pages->limit)->all();
            $category = Category::findOne($id);

            return $this->render('view', compact('products', 'pages' ,'category'));
        }
}