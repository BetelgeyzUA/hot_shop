<?php

namespace frontend\controllers;

use yii\base\Controller;
use common\models\Product;
use Yii;

class ProductController extends Controller
{
    public function actionView() {
        $id = Yii::$app->request->get('id');

        $product = Product::findOne($id);

        return $this->render('view', compact('product'));
    }

}