<?php
/**
 * Created by PhpStorm.
 * User: betelgeyz
 * Date: 03.12.16
 * Time: 11:38
 */

namespace frontend\controllers;

use yii\base\Controller;
use common\models\Product;
use frontend\models\Cart;
use common\models\Order;
use common\models\OrderItems;
use Yii;

class CartController extends Controller
{
    public function actionAdd() {
        $id = Yii::$app->request->get('id');
        $quantity = (int)Yii::$app->request->get('quantity');
        $quantity = !$quantity ? 1 : $quantity;

        $product = Product::findOne($id);
        if (empty($product)) return false;
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product, $quantity);
        if (!Yii::$app->request->isAjax) {
            return Yii::$app->response->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }

    public function actionClear() {
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.quantity');
        $session->remove('cart.sum');
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));


    }

    public function actionDelItem() {
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        $this->layout = false;
        if (Yii::$app->request->isAjax) {
            return $this->render('cart-modal', compact('session'));
        } else {
            return Yii::$app->response->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionShow() {
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }

    public function actionView() {
        $session = Yii::$app->session;
        $session->open();
        $order = new Order();
        if( $order->load(Yii::$app->request->post()) && $order->validate() ) {
             if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getId()) {
                $order->id_user = Yii::$app->user->identity->getId();
             } else {
                 $order->id_user = 4;
             };
            $order->quantity =$session['cart.quantity'];
            $order->totalPrice =$session['cart.sum'];
            if ($order->save()) {
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Ваш разаз принят');
                Yii::$app->mailer->compose('order/send', ['session' => $session])
                    ->setFrom([Yii::$app->params['adminEmail'] => 'Hot_shop'])
                    ->setTo($order->email)
                    ->setSubject('Ваш заказ принят')
                    ->send();
                $session->remove('cart');
                $session->remove('cart.quantity');
                $session->remove('cart.sum');
                return Yii::$app->response->refresh();
            } else {
                Yii::$app->session->setFlash('success', 'ошибка оформления заказа');
            }

        }
        return $this->render('view', compact('session', 'order'));
    }

    protected function saveOrderItems($items, $id_order) {
        foreach ($items as $id => $item) {
            $order_items = new OrderItems();
            $order_items->id_order = $id_order;
            $order_items->id_product = $id;
            $order_items->image = $item['img'];
            $order_items->SKU = $item['SKU'];
            $order_items->product_name = $item['title'];
            $order_items->price = $item['price'];
            $order_items->quantity_items = $item['quantity'];
            $order_items->total_price_items = $item['quantity'] * $item['price'];
            $order_items->save();
        }

    }

}