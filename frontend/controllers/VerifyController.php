<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use frontend\models\OathToken;
use yii\authclient\clients\Facebook;
use yii\authclient\clients\VKontakte;

class VerifyController extends Controller
{

    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * @param \yii\authclient\clients\Facebook $client
     */

    public function successCallback($client)
    {

        if($client instanceof Facebook){

        }
        if($client instanceof VKontakte){

        }
        $attributes = $client->getUserAttributes();

        $token = OathToken::findOne(['token' => $attributes['id']]);

        if($token){
            Yii::$app->user->login($token->user);
        } else {

            $email = empty($attributes['email']) ? null : $attributes['email'];
            $login = empty($attributes['first_name']) ? empty($attributes['name'])  ? null : $attributes['name'] : $attributes['first_name'];

            $user = new User([
                'email' => $email,
                'login' => $login,
            ]);

            if(!$user->save()) {
                var_dump($user);
                die('Error create user');
            };

            $token = new OathToken([
                'user_id' => $user->id,
                'social_id' => $client->getTitle(),
                'token' => intval($attributes['id']),
                'created_at' => time(),
            ]);

            if(!$token->save()) {
                die('Error create token');
            };

            Yii::$app->user->login($user);

            var_dump($attributes);
        }
    }

}