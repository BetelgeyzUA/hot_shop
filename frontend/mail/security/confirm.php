<?php
use yii\helpers\Url;
use yii\helpers\Html;
/**
 * @var $user \app\models\User
 */
?>


<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
<!--    <title>--><?//= Html::encode($this->title) ?><!--</title>-->
    <link rel="stylesheet" type="text/css" href="/stylesheets/email.css" />
    <?php $this->head() ?>
</head>
<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<?php $this->beginBody() ?>
<div class="content">
    <table>
        <tr>
            <td>
                <h1>Welcome, <?= $user->login ?></h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                <!-- A Real Hero (and a real human being) -->
                <p><img src="/img/fibreoptics-600x300.jpg" /></p><!-- /hero -->
                <!-- Callout Panel -->
                <p class="callout">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolor sit amet, consectetur adipisicing elit. <a class="btn btn-lg btn-success" href="<?= Url::to(['site/confirmation', 'key' => $user->code], true) ?>">Confirm</a>
                </p><!-- /Callout Panel -->

            </td>
        </tr>
    </table>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>