<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>SKU</th>
            <th>Наименование</th>
            <th>Кол-во</th>
            <th>Цена</th>
            <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($session['cart'] as $id => $item):?>
            <tr>
                <td><?= $item['SKU']?></td>
                <td><?= $item['title']?></td>
                <td><?= $item['quantity']?></td>
                <td><?= $item['price']?></td>
            </tr>
        <?php endforeach?>
        <tr>
            <td colspan="2">Итого: </td>
            <td><?= $session['cart.quantity']?></td>
            <td><?= $session['cart.sum']?></td>
            <td></td>

        </tr>
        </tbody>
    </table>