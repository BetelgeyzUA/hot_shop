<?php
/**
 * Created by PhpStorm.
 * User: betelgeyz
 * Date: 02.12.16
 * Time: 20:10
 */

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 * @property string username
 * @property string email
 * @property string $email_conf
 * @property string password
 * @property string password_conf
 * @property string $captcha
 *
 */
class RegisterForm extends Model
{
    public $username;

    public $password;

    public $password_conf;

    public $email;

    public $email_conf;

    public $captcha;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email', 'password_conf', 'email_conf', 'captcha'], 'required'],
            [['username'], 'string', 'min' => 4, 'max' => 32],
            [['email', 'email_conf'], 'email'],
            ['email_conf', 'compare', 'compareAttribute' => 'email'],
            ['password_conf', 'compare', 'compareAttribute' => 'password'],
            [['password', 'password_conf'], 'string', 'min' => 6, 'max' => 64],
            ['captcha', 'captcha'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Login',
            'password' => 'Password',
            'email' => 'E-mail',
            'password_conf' => 'Confirmation Password',
            'email_conf' => 'Confirmation E-mail',
            'captcha' => 'You Are Robot?'
        ];
    }

    /**
     * @return boolean
     */

    public  function append() {
        if($this->validate()) {

            $user = new User();
            $user->load($this->toArray(), '');
            $user->login = $this->username;

            if(!$user->save()) {
                Yii::$app->session->setFlash('Error' , 'Error save user');
                return false;
            } else {
                Yii::$app->mailer->compose('security/confirm', ['user' => $user])
                    ->setFrom([Yii::$app->params['adminEmail'] => 'My test project'])
                    ->setTo($user->email)
                    ->setSubject('Confirm')
                    ->send();
            }
        }
        return true;
    }
}