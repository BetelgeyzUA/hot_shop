<?php

namespace frontend\models;

use yii\bootstrap\Html;
use yii\db\ActiveRecord;

class Cart extends ActiveRecord
{
    public function addtoCart($product, $quantity = 1){

    if (isset($_SESSION['cart'][$product->id])) {
        $_SESSION['cart'][$product->id]['quantity'] += $quantity;
    }
    else {
        $_SESSION['cart'][$product->id] = [
            'SKU' => $product->SKU,
            'quantity' => $quantity,
            'title' => $product->title,
            'price' => $product->price,
            'img' => 'images/products/product1.jpg',
        ];

    }
        $_SESSION['cart.quantity'] = isset($_SESSION['cart.quantity']) ? $_SESSION['cart.quantity'] + $quantity : $quantity;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $quantity * $product->price : $quantity * $product->price;
    }

    public function recalc($id){
        if(!isset($_SESSION['cart'][$id])) return false;

        $quantityMinus = $_SESSION['cart'][$id]['quantity'];
        $sumMinus = $_SESSION['cart'][$id]['quantity'] * $_SESSION['cart'][$id]['price'];

        $_SESSION['cart.quantity'] -=$quantityMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        unset( $_SESSION['cart'][$id]);

    }

}