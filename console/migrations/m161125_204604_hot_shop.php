<?php

use yii\db\Migration;

class m161125_204604_hot_shop extends Migration
{
    public $users_table = 'users';

    public $token_table = 'oath_tokens';

    public $categories_table = 'categories';

    public $products_table = 'products';

    public $product_attributes_table = 'product_attributes';

    public $attributes_table = 'attributes';

    public $images_table = 'images';

    public $comments_table = 'comments';

    public $currency_table = 'currency';

    public $order_table = 'order';

    public $order_items_table = 'order_items';

    public $product_as_images_table = 'product_as_images';


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->users_table,[
            'id' => $this->primaryKey(),
            'code' => $this->string(32)->unique()->notNull(),
            'email' => $this->string(128),
            'password' => $this->string(64),
            'login' => $this->string(32),
            'phone' => $this->string(32),
            'address' => $this->string(128),
            'gender' => $this->string(12),
            'age' => $this->string(3),
            'created_at' => $this->integer()->notNull(),
            'confirmed_at' => $this->integer(),
            'blocked_at' => $this->integer(),
            'deleted_at' => $this->integer(),

        ]);

        $this->createTable($this->token_table,[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'social_id' => $this->integer()->notNull(),
            'token' => $this->string(256)->notNull(),
            'created_at' => $this->integer()->notNull()
        ]);

        $this->createTable($this->categories_table,[
            'id' => $this->primaryKey(),
            'id_parent' => $this->integer()->defaultValue(0),
            'name' => $this->string(64),
            'description' => $this->string(256),
            'date_add' => $this->integer(),
            'date_upd' => $this->integer(),
            'status' => $this->boolean(),
            'position' => $this->integer(),
        ]);

        $this->createTable($this->products_table, [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_category' => $this->integer()->defaultValue(0),
            'SKU' => $this->string(64)->notNull(),
            'title' => $this->string(256),
            'annotation' => $this->text(),
            'body' => $this->text(),
            'price' => $this->decimal(14,2),
            'position' => $this->integer(),
            'status' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable($this->product_attributes_table, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'attribute_id' => $this->integer(),
            'param' => $this->string(256)
        ]);

        $this->createTable($this->attributes_table, [
            'id' => $this->primaryKey(),
            'code' => $this->string(256),
            'name' => $this->string(256)
        ]);

        $this->createTable($this->images_table, [
            'id' => $this->primaryKey(),
            'id_user' =>$this->integer()->notNull(),
            'code' => $this->string(256),
            'name' => $this->string(256),
            'url' => $this->string(256),
            'extension' => $this->string(256),
            'size' => $this->integer()
        ]);

        $this->createTable($this->comments_table, [
            'id' => $this->primaryKey(),
            'id_parent' => $this->integer(),
            'id_product' => $this->integer(),
            'id_post' => $this->integer(),
            'id_user' => $this->integer(),
            'text' => $this->text(),
            'status' => $this->boolean()
        ]);

        $this->createTable($this->currency_table, [
            'id' => $this->primaryKey(),
            'currency_label' => $this->string(64),
            'name' => $this->string(64),
            'currency_sign' => $this->string(64),
            'exchange_rate' => $this->decimal(14,2),
            'updated_at' => $this->integer()
        ]);

        $this->createTable($this->order_table, [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'quantity' => $this->integer(),
            'totalPrice' => $this->decimal(14,2),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'email' => $this->string(128),
            'username' => $this->string(32),
            'phone' => $this->string(32),
            'address' => $this->string(128),
            'status' => $this->boolean()
        ]);

        $this->createTable($this->order_items_table, [
            'id' => $this->primaryKey(),
            'id_order' => $this->integer(),
            'id_product' => $this->integer(),
            'image' => $this->string(256),
            'SKU' => $this->string(128),
            'product_name' => $this->string(128),
            'price' => $this->decimal(14,2),
            'quantity_items' => $this->integer(),
            'total_price_items' => $this->decimal(14,2),
        ]);

        $this->createTable($this->product_as_images_table, [
            'id' => $this->primaryKey(),
            'id_image' => $this->integer(),
            'id_product' => $this->integer(),
        ]);



        $this->addForeignKey('FK_'.$this->products_table.'_'.$this->categories_table,$this->products_table, 'id_category', $this->categories_table, 'id');

        $this->addForeignKey('FK_'.$this->token_table.'_'.$this->users_table,$this->token_table, 'user_id', $this->users_table, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.$this->token_table.'_'.$this->users_table,$this->token_table);
        $this->dropForeignKey('FK_'.$this->products_table.'_'.$this->categories_table,$this->products_table);
        $this->dropTable($this->users_table);
        $this->dropTable($this->categories_table);
        $this->dropTable($this->products_table);
        $this->dropTable($this->product_attributes_table);
        $this->dropTable($this->attributes_table);
        $this->dropTable($this->images_table);
        $this->dropTable($this->comments_table);
        $this->dropTable($this->currency_table);
        $this->dropTable($this->order_table);
        $this->dropTable($this->order_items_table);
        $this->dropTable($this->product_as_images_table);
        $this->dropTable($this->token_table);
    }
}
