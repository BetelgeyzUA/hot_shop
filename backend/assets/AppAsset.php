<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/login.css',
        'css/elephant.css',
        'css/vendor.css',
        'css/application.css',
    ];
    public $js = [
        'js/jquery.cookie.js',
        'js/jquery.accordion.js',
        'js/elephant.js',
        'js/application.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
