<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="login">
    <div class="login-body">
        <a class="login-brand" href="index.html">
            <img class="img-responsive" src="/img/logo.svg" alt="Elephant Theme">
        </a>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control']) ?>

            <?= $form->field($model, 'password')->textInput(['class' => 'form-control'])->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>

        <?php ActiveForm::end(); ?>
    </div>

</div>