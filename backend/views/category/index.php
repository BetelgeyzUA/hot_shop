<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use common\models\Category;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'id_parent',
                'value' => function($data){
                    if(isset($data->category)) {
                        return $data->category->name;
                    }
                    return '<span class="text-danger">No parent</span>';
                },
                'format' => 'html',
            ],
            'name',
            'description',
            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'options' => ['placeholder' => 'Start date'],
                    'options2' => ['placeholder' => 'End date'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ]),
                'attribute' => 'date_upd',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'status',
                'contentOptions' => ['class' => 'status'],
                'format' => 'html',
                'filter' => [
                    1 => 'Active',
                    0 => 'Inactive',
                ],
                'value' => function ($data) {
                    switch ($data->status) {
                        case 0: return '<span class="label label-danger">Inactive</span>';
                            break;
                        case 1: return '<span class="label label-success">Active</span>';
                            break;
                    }
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
