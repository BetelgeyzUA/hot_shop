<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h2><?= 'View Order №'.Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_user',
                'value' => $model->user->login,
            ],
            'quantity',
            [
                'attribute' => 'totalPrice',
                'format' => ['Currency', '$'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            'email:email',
            'username',
            'phone',
            'address',
            [
                'attribute' => 'status',
                'value' => $model->getStatus($model),
                'format' => 'html',
            ]
//            'status',
        ],
    ]) ?>

    <?php $items = $model->orderItems;?>

    <?php if($items): ?>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Фото</th>
                    <th>Наименование</th>
                    <th>SKU</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($items as $id => $item):?>
                    <tr>
                        <td><?= Html::img(Yii::$app->params['UrlFront'].$item->image, ["alt" => $item->product_name ]) ?></td>
                        <td><?= $item->product_name ?></td>
                        <td><?= $item->SKU ?></td>
                        <td><?= $item->quantity_items ?></td>
                        <td><?= $item->price ?></td>
                        <td><?= $item->quantity_items * $item->price ?></td>
                    </tr>
                <?php endforeach?>
                <tr>
                    <td colspan="3">Итого: </td>
                    <td colspan="2"><?= $model->quantity ?></td>
                    <td><?=  $model->totalPrice ?></td>
                    <td></td>

                </tr>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <h3>Корзина пуста</h3>
    <?php endif;?>


</div>
