<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'table-order',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'user.login',
                'label' => 'User Name'
            ],
            'quantity',
            'totalPrice',
            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'options' => ['placeholder' => 'Start date'],
                    'options2' => ['placeholder' => 'End date'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ]),
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'status',
                'contentOptions' => ['class' => 'status'],
                'format' => 'html',
                'filter' => [
                    0 => 'active',
                    1 => 'Accepted',
                    2 => 'Completed',
                    3 => 'Rejected',
                ],
                 'value' => function ($data) {
                    switch ($data->status) {
                        case 0: return '<span class="label label-primary">active</span>';
                        break;
                        case 1: return '<span class="label label-info">Accepted</span>';
                            break;
                        case 2: return '<span class="label label-success">Completed</span>';
                            break;
                        case 3: return '<span class="label label-danger">Rejected</span>';
                            break;
                    }
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
