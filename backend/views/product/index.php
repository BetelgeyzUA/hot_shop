<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
//            'id_category',
            [
                'attribute' => 'category',
                'value' => 'category.name',
            ],
            'SKU',
            'title',
            // 'annotation:ntext',
            // 'body:ntext',
             'price',
            // 'position',
//            'updated_at',
            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'options' => ['placeholder' => 'Start date'],
                    'options2' => ['placeholder' => 'End date'],
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ]),
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'status',
                'contentOptions' => ['class' => 'status'],
                'format' => 'html',
                'filter' => [
                    1 => 'Active',
                    0 => 'Inactive',
                ],
                'value' => function ($data) {
                    switch ($data->status) {
                        case 0: return '<span class="label label-danger">Inactive</span>';
                            break;
                        case 1: return '<span class="label label-success">Active</span>';
                            break;
                    }
                }
            ],

            // 'created_at',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
