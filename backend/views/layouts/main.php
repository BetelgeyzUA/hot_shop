<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="layout layout-header-fixed">
<?php $this->beginBody() ?>
<div class="layout-header">
    <div class="navbar navbar-default">
        <div class="navbar-header navbar-header-z">
            <a class="navbar-brand navbar-brand-center" href="<?= Url::home();?>">
                <?= Html::img('@web/img/logo-inverse.svg', ['alt' => 'Elephant', 'class' => 'navbar-brand-logo']) ?>
            </a>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
                <span class="sr-only">Toggle navigation</span>
                <span class="bars">
                    <span class="bar-line bar-line-1 out"></span>
                    <span class="bar-line bar-line-2 out"></span>
                    <span class="bar-line bar-line-3 out"></span>
                 </span>
                <span class="bars bars-x">
                     <span class="bar-line bar-line-4"></span>
                     <span class="bar-line bar-line-5"></span>
                 </span>
            </button>
        </div>
        <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">
                <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="bars">
                        <span class="bar-line bar-line-1 out"></span>
                        <span class="bar-line bar-line-2 out"></span>
                        <span class="bar-line bar-line-3 out"></span>
                        <span class="bar-line bar-line-4 in"></span>
                        <span class="bar-line bar-line-5 in"></span>
                        <span class="bar-line bar-line-6 in"></span>
                    </span>
                </button>
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-xs hidden-sm">
                        <form class="navbar-search navbar-search-collapsed">
                            <div class="navbar-search-group">
                                <input class="navbar-search-input" type="text" placeholder="Search for people, companies, and more&hellip;">
                                <button class="navbar-search-toggler" title="Expand search form ( S )" aria-expanded="false" type="submit">
                                    <span class="icon icon-search icon-lg"></span>
                                </button>
                                <button class="navbar-search-adv-btn" type="button">Advanced</button>
                            </div>
                        </form>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
                          <span class="icon-with-child hidden-xs">
                            <span class="icon icon-envelope-o icon-lg"></span>
                            <span class="badge badge-danger badge-above right">1</span>
                          </span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
                          <span class="icon-with-child hidden-xs">
                            <span class="icon icon-bell-o icon-lg"></span>
                            <span class="badge badge-danger badge-above right">1</span>
                          </span>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                            <img class="rounded" width="36" height="36" src="/img/0180441436.jpg" alt="Teddy Wilson">
                        <?php echo Nav::widget([
                        'options' => ['class' => 'navbar-account-btn', 'id' => 'top-menu'],
                        'items' => [
                        Yii::$app->user->isGuest ? (
                        ['label' => 'Login', 'url' => ['/site/login'], ]
                        ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'navbar-account-btn', 'id' => 'admin_logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                    ],
                    ]);

                    ?>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="layout-main">
        <div class="layout-sidebar">
            <div class="layout-sidebar-backdrop"></div>
                <div class="layout-sidebar-body">
                    <div class="custom-scrollbar">
                        <nav id="sidenav" class="sidenav-collapse collapse">
                            <ul class="sidenav" id = 'left_admin_menu'>
                                <li class="sidenav-heading">Navigation</li>
                                <li class="sidenav-item has-subnav">
                                    <a href="#" aria-haspopup="true">
                                        <span class="sidenav-icon icon icon-shopping-cart"></span>
                                        <span class="sidenav-label">Orders</span>
                                    </a>
                                    <ul class="sidenav-subnav collapse">
                                        <li class="sidenav-subheading">Orders</li>
                                        <li><?= Html::a('View Orders', ['order/index']) ?></li>
                                    </ul>
                                </li>
                                <li class="sidenav-item has-subnav">
                                    <a href="#" aria-haspopup="true">
                                        <span class="sidenav-icon icon icon-list"></span>
                                        <span class="sidenav-label">Categories</span>
                                    </a>
                                    <ul class="sidenav-subnav collapse">
                                        <li class="sidenav-subheading">Orders</li>
                                        <li><?= Html::a('View Category', ['category/index']) ?></li>
                                        <li><?= Html::a('Add Category', ['category/create']) ?></li>

                                    </ul>
                                </li>
                                <li class="sidenav-item has-subnav">
                                    <a href="#" aria-haspopup="true">
                                        <span class="sidenav-icon icon icon-th"></span>
                                        <span class="sidenav-label">Product</span>
                                    </a>
                                    <ul class="sidenav-subnav collapse">
                                        <li class="sidenav-subheading">Product</li>
                                        <li><?= Html::a('View Product', ['product/index']) ?></li>
                                        <li><?= Html::a('Add Product', ['product/create']) ?></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
             </div>
        <div class="layout-content">
            <div class="container-admin">
                <?= $content ?>
            </div>
        </div>
        <div class="layout-footer"></div>

    </div>
    <div class="theme"> </div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
