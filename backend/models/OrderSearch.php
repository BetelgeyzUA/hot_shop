<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{

    public $date_from;
    public $date_to;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity', 'status'], 'integer'],
            [['totalPrice', 'id'], 'number'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
            [['email', 'username', 'phone', 'address', 'id_user', 'updated_at', 'created_at', 'user.login'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['user.login']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_ASC
                ]
            ]
        ]);

        $query->joinWith(['user' => function($query) { $query->from(['user' => 'users']); }]);


        $dataProvider->sort->attributes['user.login'] = [
            'asc' => ['user.login' => SORT_ASC],
            'desc' => ['user.login' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order.id' => $this->id,
//            'id_user' => $this->id_user,
//            'quantity' => $this->quantity,
//            'totalPrice' => $this->totalPrice,
//            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['LIKE', 'quantity', $this->quantity])
        ->andFilterWhere(['LIKE', 'totalPrice', $this->totalPrice])
        ->andFilterWhere(['LIKE', 'user.login', $this->getAttribute('user.login')])
        ->andFilterWhere(['>=', 'updated_at', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
        ->andFilterWhere(['<=', 'updated_at', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address]);
//            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
