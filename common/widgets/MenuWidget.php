<?php

namespace common\widgets;
use yii\base\Widget;
use common\models\Category;
use Yii;


class MenuWidget extends Widget{

    public $tpl;
    public $data;
    public $model;
    public $tree;
    public $menuHtml;

    public function init() {
        parent::init();

        if(!(isset($this->tpl))) {
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

    public function run() {

        //get_Cache
        if($this->tpl == 'menu.php'){
            $menu = Yii::$app->cache->get('menu');
            if($menu) return $menu;
        }

        $this->data = Category::find()->indexBy('id')->where(['status' => 1])->asArray()->all();
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);

        //set_Cache;
        if($this->tpl == 'menu.php'){
            Yii::$app->cache->set('menu', $this->menuHtml, 60);
        }

        return $this->menuHtml;
    }

    protected function getTree() {
        $tree = [];
        foreach ($this->data as $id=>&$node) {
            if (!$node['id_parent']) {
                $tree[$id] = &$node;
            } else {
                $this->data[$node['id_parent']]['childs'][$node['id']] = &$node ;
            }
        }

        return $tree;
    }

    protected function getMenuHtml($tree, $tab = '') {
        $str ='';
        foreach ($tree as $category) {
            $str .= $this->catToTamplate($category, $tab);
        }
        return $str;
    }

    protected function catToTamplate ($category, $tab) {
        ob_start();
        include __DIR__.'/menu_tpl/'.$this->tpl;
        return ob_get_clean();
    }

}