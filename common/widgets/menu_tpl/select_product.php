<?php
/* @var $category common\models\Category */
?>
<option
    value="<?= $category['id']?>"
    <?php if($category['id'] == $this->model->id_category) echo ' selected'?>
><?= $tab . $category['name']?></option>
<?php if( isset($category['childs']) ): ?>
    <ul>
        <?= $this->getMenuHtml($category['childs'], $tab . '-')?>
    </ul>
<?php endif;?>