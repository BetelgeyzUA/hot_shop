<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\IntegrityException;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $code
 * @property string $login
 * @property string $email
 * @property string $password
 * @property integer $created_at
 * @property integer $confirmed_at
 * @property integer $bloked_at
 * @property integer $deleted_at
 * @property OathToken[] array $outhTokens
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public function getOrder() {
        return $this->hasOne(Order::className(), ['id_user' => 'id']);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'default', 'value' => time()],
            [['created_at', 'code'], 'required'],
            [['created_at', 'confirmed_at', 'blocked_at', 'deleted_at', 'confirmed_at'], 'integer'],
            [['login'], 'string', 'min' => 2,'max' => 32],
            [['code'], 'string', 'max' => 32],
            [['email'], 'email'],
            [['password'], 'string', 'max' => 64],
        ];
    }


    public function beforeValidate()
    {
        if($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            $this->code = md5(microtime().self::className());
        }
        return parent::beforeValidate();
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'email' => 'Email',
            'password' => 'Password',
            'created_at' => 'Created At',
            'confirmed_at' => 'Confirmed At',
            'blocked_at' => 'Blocked_at At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOathTokens()
    {
        return $this->hasMany(OathToken::className(), ['user_id' => 'id']);
    }

    public function getUsername() {
        return $this->login;
    }


    public function save($runValidation = true, $attributeNames = null) {
        $result = false;
        try{
            $result =  parent::save($runValidation, $attributeNames);
        }catch (IntegrityException $e){
            Yii::$app->session->setFlash('error_email', 'This email is not correct! duplicate email');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['login' => $username])->orWhere(['email' => $username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return true;
    }

    /**
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

}