<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $code
 * @property string $name
 * @property string $url
 * @property string $extension
 * @property integer $size
 */
class Image extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'default', 'value' => Yii::$app->user->getId()],
            [['id_user'], 'required'],
            [['size', 'id_user'], 'integer'],
            [['code', 'name', 'url', 'extension'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'User',
            'code' => 'Code',
            'name' => 'Name',
            'url' => 'Url',
            'extension' => 'Extension',
            'size' => 'Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_category']);
    }
}

