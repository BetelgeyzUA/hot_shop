<?php

namespace common\models;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "order_items".
 *
 * @property integer $id
 * @property integer $id_order
 * @property integer $id_product
 * @property integer $image
 * @property string $SKU
 * @property string $product_name
 * @property string $price
 * @property integer $quantity_items
 * @property string $total_price_items
 */
class OrderItems extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_items';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order', 'id_product', 'quantity_items'], 'integer'],
            [['price', 'total_price_items'], 'number'],
            [['SKU', 'product_name'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 256],
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'id_order']);
    }

}
