<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\models\Category;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $id_category
 * @property integer $id_user
 * @property string $SKU
 * @property string $title
 * @property string $annotation
 * @property string $body
 * @property string $price
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Categories $idCategory
 */
class Product extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }

    public function getStatus($data) {
        switch ($data->status) {
            case 0: return '<span class="label label-danger">Inactive</span>';
                break;
            case 1: return '<span class="label label-success">Active</span>';
                break;
        }
    }

    public function getName($data) {

        if (isset($data->category->name)) {
            return $data->category->name;
        } else {
            return 'No Parent';
        }


    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category', 'position', 'status', 'created_at', 'updated_at', 'id', 'id_user'], 'integer'],
            [['id_user'], 'default', 'value' => Yii::$app->user->getId()],
            [['SKU', 'id_user'], 'required'],
            [['annotation', 'body'], 'string'],
            [['price'], 'number'],
            [['SKU'], 'string', 'max' => 64],
            [['title'], 'string', 'max' => 256],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['id_category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'User',
            'id_category' => 'Category',
            'SKU' => 'SKU',
            'title' => 'Title',
            'annotation' => 'Annotation',
            'body' => 'Body',
            'price' => 'Price',
            'position' => 'Position',
            'status' => 'Status',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
            'category' => 'Category'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }
}
