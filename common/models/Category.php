<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $name
 * @property string $description
 * @property integer $date_add
 * @property integer $date_upd
 * @property integer $status
 * @property integer $position
 */

class Category extends ActiveRecord {

    /**
     * @inheritdoc
     */

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_add', 'date_upd'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date_upd'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'categories';
    }

    public function getProduct() {
        return $this->hasMany(Product::className(), ['id_category' => 'id']);
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'id_parent']);
    }

    public function getStatus($data) {
        switch ($data->status) {
            case 0: return '<span class="label label-danger">Inactive</span>';
                break;
            case 1: return '<span class="label label-success">Active</span>';
                break;
        }
    }

    public function getName($data) {

        if (isset($data->category->name)) {
            return $data->category->name;
        } else {
            return 'No Parent';
        }


    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 64],
            [['description'], 'string', 'max' => 256],
            ['status', 'in', 'range' => ['0', '1'], 'strict' => true],
            [['id','id_parent', 'date_add', 'date_upd', 'position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Category №',
            'id_parent' => 'Parent category',
            'name' => 'Category name',
            'description' => 'Description',
            'date_add' => 'Create date',
            'date_upd' => 'Update date',
            'status' => 'Status',
            'position' => 'Position',
        ];
    }
}