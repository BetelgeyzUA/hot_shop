<?php

namespace common\models;

use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $quantity
 * @property string $totalPrice
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 * @property string $email
 * @property string $username
 * @property string $phone
 * @property string $address
 */
class Order extends ActiveRecord
{
    /**
     * @inheritdoc
     */

public function behaviors() {
    return [
        [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
            ],
        ],
    ];
}


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    public function getOrderItems() {
        return $this->hasMany(OrderItems::className(), ['id_order' => 'id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getStatus($data) {
        switch ($data->status) {
            case 0: return '<span class="label label-primary">active</span>';
                break;
            case 1: return '<span class="label label-info">Accepted</span>';
                break;
            case 2: return '<span class="label label-success">Completed</span>';
                break;
            case 3: return '<span class="label label-danger">Rejected</span>';
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'address', 'username', 'phone' ], 'required'],
            [['email'], 'email'],
            ['username', 'match', 'pattern' => '/^[a-zA-Zа-яёА-ЯЁ\s]+$/'],
            ['phone', 'match', 'pattern' => '/^\+?[0-9]{7,12}$/'],
            [['email', 'address'], 'string', 'max' => 128],
            [['username', 'phone'], 'string', 'max' => 32],
            [['id_user', 'quantity'], 'integer'],
            ['status', 'in', 'range' => ['0', '1', '2', '3'], 'strict' => true],
            [['totalPrice'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Order №',
            'updated_at' => 'Updated Order',
            'created_at' => 'Created Order',
            'id_user' => 'User Name',
            'email' => 'E-mail',
            'username' => 'Customer Name',
            'phone' => 'Phone',
            'address' => 'Address',
        ];
    }

}
