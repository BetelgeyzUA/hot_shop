<?php
return [
    'adminEmail' => 'betelgeyzua@gmail.com',
    'supportEmail' => 'betelgeyzua@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'aliases' => [
        '@frontsite' => 'http://shop.host.com',
        '@adminsite' => 'http://admin.shop.host.com'
    ]
];
