<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

         'urlManager' => [
             'enablePrettyUrl' => true,
             'showScriptName' => false,
             'rules' => [
                 'category/<id:\d+>/pages/<page:\d>' => 'category/view',
                 'category/<id:\d+>' => 'category/view',
                 'product/<id:\d+>' => 'product/view'
             ],
         ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'spalahtest',
                'password' => 'sr4246sr',
                'port' => '465',
                'encryption' => 'ssl',
            ],
            'useFileTransport' => false,
        ],

        // authClientCollection vkontakte
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '5758455',
                    'clientSecret' => 'HnbZEPFobmsGiEBeyrTu',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '215209485555230',
                    'clientSecret' => '05e0a044647dc04706d2cd02ed9e4fc8',
                ],
            ],
        ],



    ],
];
